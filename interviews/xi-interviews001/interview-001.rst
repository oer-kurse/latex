Schreiben einer Dissertation
============================

.. |a| image:: ./images/paulilogo.jpg
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     - Joachim, Du hast gerade den Doktortitel bekommen und dies durch
       eine wissenschaftliche Arbeit belegt. Ich möchte Dir ein paar
       Fragen stellen, die vielleicht für Neueinsteiger ganz nützlich
       sein könnten.


**Peter Koppatz**

Erste Frage: War es schwer, ein Thema zu finden? Wie
viele Themen hast Du in Betracht gezogen? Hat die
Formulierung des Themas einen evolutionären Prozess
durchlaufen oder war es mit der ersten Formulierung
schon perfekt?
		
**Joachim Scholz**

Ich hatte einige Themen im Kopf, aber schwierig war
es, ein solches Thema zu finden, mit dem man – wie
das für eine Dissertation gefordert wird - ein
Forschungsgebiet erweitern und Neuland beschreiten
kann. Im Studium erfährt man, wie aufwändig es sein
kann, den Forschungsstand auf einem Wissensgebiet
nachzuvollziehen, noch schwerer ist es aber
abzuschätzen, wo es in der Welt des Wissens noch
dunkel ist. Man bewegt sich ja immer auf einem schon
mehr oder weniger beackerten Feld, auf dem es immer
schon Erfahrenere gibt als einen selbst. Im Gespräch
mit meinem Doktorvater habe ich zu meinem Thema
gefunden. Es hat einige Zeit gedauert, bis es ganz zu
»meinem« Thema wurde. An den vielen Umstellungen
meiner Gliederung kann ich heute ablesen, wie zäh es
dabei voran ging.

Was in der letzten Textfassung jetzt ziemlich
leichtfüßig daherkommt, war in den ersten Texten ein
unsicheres Stolpern.

Bei Studienarbeiten geht meist »nur« um die
nachvollziehbare Aufarbeitung des Wissensstandes auf
einem Gebiet.

**Peter Koppatz**

Es heißt oft: Zeit ist Geld. Deshalb ein paar Fragen
zum Zeitmanagement: Wie lange hast Du an der Arbeit
geschrieben, insgesamt, pro Monat, pro Tag? Welche
Arbeitsschritte waren die zeitintensivsten? Kannst Du
eine Empfehlung geben?

**Joachim Scholz**

Ehrlich gesagt, sind sieben Jahre von der
Themenfindung bis zur Abgabe der Arbeit ins Land
gegangen, in denen ich aber neben der Dissertation
auch andere Aufgaben hatte. Sehr viel Zeit hat es
gekostet, im Archiv die Quellen für meine historische
Arbeit abzuschreiben. Die Kopierkosten sind dort
unerschwinglich.

Wie man sich das Schreiben selbst zeitlich
organisiert, muss jeder für sich herausfinden. Ich
kann (leider) abends am besten arbeiten und schaffe es
nie, mehr als fünf Stunden am Tag effektiv zu
schreiben. Aber die Ablenkung scheint irgendwie auch
zur Arbeit zu gehören. Was in diesen eigentlich
„unproduktiven“ Phasen an Produktivem passiert ist,
lässt sich schwer beschreiben. Wenn ich mich an manche
plötzliche  Vorwärtsbewegung erinnere – als ich einmal
eine richtig gute Seite in zwanzig Minuten fertig
hatte oder meine Gliederung »revolutionierte« – war
das sicher unbewusst lange vorbereitet. Und dafür gibt
es keine Empfehlung oder Technik. Das soll aber nicht
heißen, dass man keine Möglichkeiten hat, die Arbeit
durch Organisation besser zu strukturieren.

Bevor man sein Thema findet, muss man sich ja selbst
zu einem Experten auf seinen Gebiet machen. Um einen
guten Überblick über den Stand des Wissens zu
bekommen, ist eine eigene Literaturdatenbank sicher
nützlich.

**Peter Koppatz**

Wie hast Du die Quellen analysiert und verwaltet?
		
**Joachim Scholz**
Relativ konventionell. In einem Textdokument habe ich
Literatur gesammelt und Kopien meiner Texte und
Quellen alphabetisch in Ordner geheftet. Auch
Karteikarten habe ich benutzt. Da ich viele
Archivtexte zwangsläufig durch Abschreiben
digitalisiert hatte, ließen die sich später wunderbar
nach Stichworten durchforsten. Jede digital
vorliegende Quelle war ein großer Vorteil. Insgesamt
hat sich für mich aber ein Mix aus herkömmlichen
Verfahren und modernen bewährt.

**Peter Kopatz**

Mit welchem Programm hast Du Deine Arbeit geschrieben? Da eine
Bachelor- oder Doktorarbeit sehr umfangreich werden
kann, hat man sicher mit einigen technischen
Problemen zu kämpfen. An welche erinnerst Du Dich
noch besonders und wie hast Du sie gelöst?

**Joachim Scholz**

Ich habe »Word« 2007 benutzt und bin eigentlich zufrieden
damit. Angeblich soll das Layout in anderen
Programmen ansprechender sein, aber es gibt auch in
Word viele Möglichkeiten, ein schönes Druckbild zu
erreichen. Probleme entstehen nach meiner Erfahrung
bei großen Dokumenten durch unerklärliche
Neuberechnung der Umbrüche. Ich konnte mir beim
abschließenden Ausdruck nicht erklären, warum die
Arbeit plötzlich 309 Seiten hatte statt 311, nur weil
ich auf Seite 200 ein Komma getilgt hatte. Hier hilft
nur ein Aufsplitten des Dokuments in möglichst viele
Filialdokumente, die man dann zur Erstellung des
Inhaltsverzeichnisses miteinander verknüpft. Auch
diese Prozedur wird für den Laien zur Zitterpartie
und ist mir nicht ohne Improvisation gelungen. Ich
habe die Seitenzahlen »manuell« eingetragen, weil die
automatische Berechnung immer daneben lag.


Ein anderes Problem entstand durch problematisch positionierte
Fußnoten. Steht ein Fußnotenzeichen im Fließtext auf
einer der letzten Zeilen der Seite und wird die
Fußnote so lang, dass das Fußnotenzeichen im
Fließtext dadurch auf die nächste Seite verschoben
würde, bekommt das Programm keinen sauberen
Seitenumbruch hin. Da hilft nur ein Kürzen oder
Versetzen der Fußnote um mehrere Zeilen nach oben,
was inhaltlich oft nicht passt. Für dieses Problem
habe ich keine richtige Lösung gefunden.

Sicher ist man bei Word jedenfalls erst, wenn man ein
PDF erstellt hat. Für das Programm spricht aber die
Benutzerfreundlichkeit im Normalfall.

**Peter Koppatz**

Welche formalen Kriterien sollte man Deiner Meinung
nach besonders beachten?
		 
**Joachim Scholz**

Formale Kriterien sollte man meiner Meinung nach sehr
ernst nehmen, weil sie der Teil der Arbeit sind, den
man am besten kontrollieren kann. Dass man eine
schriftliche Arbeit vom Anfang bis zum Ende kreativ
und intelligent verfasst, lässt sich nie garantieren,
aber formale Fehler sind in jedem Fall vermeidbare
Fehler. Hier hat man alles in der Hand, wenn man ein
paar Regeln beachtet. Hier nur drei Schwerpunkte:

  
1. Rechtschreibung und Grammatik sagen viel darüber aus, wie
   sorgfältig man gearbeitet hat. Jede Arbeit, die
   man abgibt, sollte man zuvor selbst mehrmals
   gründlich korrekturgelesen haben, sie aber auch
   einem Dritten zum Korrekturgang gegeben haben, von
   dem/r man weiß, dass er/sie in Rechtschreibfragen
   bewandert und zuverlässig ist.
   
   
2. Alle fremden Informationen, die in die Arbeit
   eingeflossen sind, gründlich zu belegen, ist
   mindestens ebenso wichtig. Welche Zitationsregeln
   einzuhalten sind, erfährt man vom Verlag, bzw. an
   der Uni vom jeweiligen Dozenten. Es gibt viele
   gute Leitfäden, z.B. in diesem Kurs. Das
   Literaturverzeichnis am Schluss der Arbeit muss
   lückenlos alle herangezogenen Quellen
   enthalten. Von Internetquellen, bei denen es sich
   nicht selbst um wissenschaftliche
   Veröffentlichungen handelt, sollte man absehen, um
   nicht den Eindruck zu vermitteln, flüchtig und
   unsauber gearbeitet zu haben.
	  
	  
3. Zu den formalen Kriterien gehören auch
   Gestaltungsfragen: Von Randbreiten,
   Zeilenabständen und Schriftgrößen bis zur
   Auflösung eingescannter Bilder, der korrekten
   Beschriftung von Abbildungen und Tabellen gibt es
   immer wieder Zweifelsfälle. Sichere Orientierung
   bieten meist die Standards einer anerkannten
   Fachzeitschrift.
   
**Peter Koppatz**

Wenn alles fertig ist, muss das Werk ja noch gedruckt
werden. Was sind Deine Erfahrungen bei der Umsetzung
gewesen?


**Joachim Scholz**

Für den Ausdruck der Arbeit würde ich ein
PDF-Dokument erstellen und es in einem guten Copyshop
drucken und binden lassen. Das ist aber noch nicht
der Druck für eine Veröffentlichung, die erst bei der
Dissertation verlangt wird. Und selbst da reicht die
Veröffentlichung auf CD und der Ausdruck für die
Universitätsbibliothek.


Meine Dissertation ist noch nicht gedruckt. Momentan
geht es darum, den passenden Verlag zu finden. Beim
Druck meiner Magisterarbeit und bei der Begleitung
anderer Druckprojekte war ich erstaunt, welche enorme
Sorgfalt Verlage auf die Einhaltung formaler
Kriterien legen können.

**Peter Koppatz**

Schön dass Du die Fragen so ausführlich beantwortet hast. 
Viel Erfolg privat wie beruflich. Ich hoffe, Du hast
jetzt wieder mehr Zeit für http://www.paulinenaue.info

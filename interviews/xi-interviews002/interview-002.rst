Der Weg zur Druckerei
=====================

.. |a| image:: ./images/print-express.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
	  
     - Der Druck ist sozusagen die  »Entbindung«, denn ein neues Werk
       erblickt das Licht der Welt und nichts kann mehr zurückgenommen
       werden, selbst die Fehler nicht!

       Lesen Sie das Interview mit Frau Beyer, der Geschäftsführerin
       der Firma Print Express (www.print-potsdam.de).


   
**Peter Koppatz**

Frau Beyer, Sie haben langjährige Erfahrungen durch
Ihre Tätigkeit im Druckgewerbe, auch wird Ihre
Serviceleistung von Studenten in Potsdam gern in
Anspruch genommen. Das es da nicht immer glatt geht,
kann ich mir gut vorstellen.
		
Ein Student möchte eine Bachelor- oder Diplomarbeit drucken
lassen. Wann gibt es aus Ihrer Sicht Probleme, die
vermieden werden könnten?


**Frau Beyer**

Normalerweise haben wir wenig Probleme, aber von einhundert Fällen
sind zehn schon als schwierig einzustufen und verursachen beim
Studenten wie bei meinen Mitarbeitern Mehrarbeit und unnötigen
Stress. Besonders häufig entstehen Probleme, wenn die Arbeit im
Word-Format abgegeben wird. Unsere Programme brechen die
Seiten oft anders um als das von den Studenten verwendete Word.

**Peter Koppatz**

Dann ist es vielleicht besser die Arbeit im PDF-Format abzugeben?

**Frau Beyer**

Ja, auf jeden Fall. Aber auch hier muss nach der
Konvertierung jede Seite nochmals kontrolliert
werden. Man kann sich auf die Automatik der Programme
nicht verlassen.
Oft sind es einzelne Zeilen oder Fußnoten mit viel Text,
die das Programm auf unerwünschte Art und Weise anders
platziert als vom Autor gewünscht.

**Peter Koppatz**

Für einen Studenten lässt sich der zeitliche Aufwand
für den Druck schlecht einschätzen. Wie hoch ist er?
       
**Frau Beyer**
Man sollte schon ein wenig Zeit einplanen und nicht
denken, man sei der einzige Kunde. Besonders kritisch ist
es, wenn die Öffnungszeiten nicht beachtet werden. Wenn
wir sonnabends um 14:00 Uhr schließen und 13:00 Uhr kommt
jemand mit so einem komplexen Werk, ist das mit
Überstunden unserer Mitarbeiter verbunden. Wir sind hier
schon sehr gut; wenn es abgesprochen ist, kann man seine
Arbeit für gewöhnlich schon am selben Tag gedruckt und
gebunden mit nach Hause nehmen. Andere Anbieter liefern
nicht unter zwei Tagen! Unser Ziel ist es, dass ein
zufriedener und glücklicher Kunde unser Geschäft
verlässt.

Erst vor wenigen Tagen kam eine Studentin, deren Word-Datei noch
einmal komplett neu formatiert werden musste, das hat über drei
Stunden gedauert. In dieser Zeit hat sie einen
Arbeitsplatz und einen PC blockiert. Das dürfen wir dann
kostenlos zur Verfügung stellen und unser Mitarbeiter
darf dann ins Schwimmbad gehen. Nein, im Ernst:
andere Kundenaufträge verschieben sich dann auch oder die
Mitarbeiter müssen Überstunden leisten, es ist also sehr
egoistisch, schlecht vorbereitet zu sein und ich frage
mich manchmal, was haben die jungen Leute während des
Studiums überhaupt gelernt?


**Peter Koppatz**

Tja, grau ist alle Theorie! Was würden Sie empfehlen,
wenn es gut laufen soll?
		
**Frau Beyer**

Viele Studenten machen es richtig. Sie kommen zweimal in unsere
Druckerei. Beim ersten Mal erkundigen sie sich, wie viel der Druck
kostet und  wählen auch schon das richtige Papier
aus. Für das Binden bieten wir unterschiedliche Lösungen
an, die können hier ebenfalls schon betrachtet
werden. Wenn diese Fragen geklärt sind, sollte man
einen Termin vereinbaren, der auch noch Spielraum für
unvorhergesehene Ereignisse lässt. Für die Druckdaten ist
unser Grafiker gern auch bereit, die notwendigen Hinweise
zu geben. Eine Konvertierung von Word ins PDF-Format ist
in unserem Hause ebenfalls möglich – bitte den
Zeitaufwand beachten!

Wenn Bilder im Word-Dokument enthalten sind, dann direkt
in das Dokument eingebettet.

**Peter Koppatz**

Vielen Dank für die Informationen. Ich hoffe, das die
Zahl der gut vorbereiteten Studenten in Ihrer
Druckerei weiter zunimmt.
		



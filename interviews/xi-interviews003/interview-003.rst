==========================
Wissenschaftler & Reviewer
==========================

.. |a| image:: ./images/pyhasse.jpg
   :width: 150px
   :alt: PyHasse-Projekt
	 
.. list-table::
   :widths: 30 60
   :class: lernziel	    

   * - |a|

        PyHasse-Projekt
     
	  
     - Eine wissenschaftliche Arbeit durchläuft einen oder mehrere
       Reviewprozesse. Dr. Rainer Brüggemann ist seit vielen Jahren
       nicht nur Wissenschaftler und Autor, er nimmt auch die Mühen
       eines Reviewers wahr. Zu dieser Tätigkeit habe ich ihm folgende
       Fragen gestellt:

   
**Peter Koppatz**

Warum nimmst Du die Aufgabe eines Reviewers wahr?

**Rainer Brüggemann**

Ich halte es für eine Ehrenaufgabe. Letztlich hat der Steuerzahler
viel Geld in mich hineingesteckt; und eine Möglichkeit, mich dafür
erkenntlich zu zeigen, ist zu versuchen mitzuhelfen, die Qualität
wiss. Arbeiten zu verbessern.

**Peter Koppatz**

Wie lange bist Du als Reviewer tätig?

**Rainer Brüggemann**

Seit ca 1972 (kann auch schon 71 gewesen sein)

**Peter Koppatz**

Was sind die Aufgaben eines Reviewer?
       
**Rainer Brüggemann**

Passt die Arbeit in das Journal. Z.B. MATCH, eine
mathemat. Chemie-Zeitschrift: Hier passen Arbeiten zur Qualität von
Datenbanken nicht hinein, - auch wenn die Datenbanken chemische
Inhalte aufweisen.

- Qualität der Arbeit: Kann man den Ausführungen folgen? Wo
  braucht der Leser Hilfestellung. Motto: Wenn ich es
  nicht kapiere, so könnte es sein, dass auch andere
  damit Probleme haben.
- Untergeordnet: Technische Qualität. Sind
  Abbildungen/Tabellen verständlich, Druckfehler (ich
  suche nie spezifisch nach Druckfehlern; aber wenn mir
  welche auffallen notiere ich sie).
- Abstrakt klar? Titel klar? Keywords klar? Referenzen
  einheitlich? (Die verschiedenen Zeitschriften
  verlangen sehr spezielle Formate. Das sehe ich nicht
  als eine Aufgabe des Reviewer an, die Einhaltung
  dieser speziellen Formate zu überprüfen. Aber die
  Angaben zu Referenzen sollten in sich einheitlich
  sein.

  Nicht einmal
  
  ::

     R. Bruggemann, P.Koppatz, ###, 2014

  und einmal

  ::
  
     R.Bruggemann, P.Koppatz, 2014, ###.
  

**Peter Koppatz**

Kannst Du Dir die Arbeiten aussuchen oder machen die Verlage Vorgaben?

**Rainer Brüggemann**

Verschiedene Verlage schicken mir Arbeiten zu mit der Bitte um eine
Begutachtung. Sie können auf meinen Namen „so“ gekommen sein; oder aber
Autoren empfehlen mich spezifisch für ihre Arbeit. Ich lehne
(inzwischen) Arbeiten ab, von denen ich glaube, nicht die Expertise zu
haben. Früher habe ich mich, wenn es nur irgendwie ging, auch in
fremde (chemische) Gebiete eingearbeitet, wenn das Thema reizvoll war.

**Peter Koppatz**

Mir geht es um Hinweise an die Autoren. Was stört Dich an Arbeiten,
die Du bewertest?

**Rainer Brüggemann**

Das kann ich nicht allgemein beantworten. Z.B. wenn eine mathematisch
orientierte Zeitschrift mir was schickt, dürfen die Autoren auch
strikte mathematische Notation benützen, Ist aber eine Zeitschrift
nicht speziell mathematisch ausgerichtet, dann mahne ich eher an, man
möchte doch bitte an den allgemeinen Leser denken. Ansonsten ärgert
mich; Notationswechsel. Eine Variable, aber mehrere Namen (das
passiert sehr leicht und gelegentlich auch mir), Wiederholungen, und
zu lange Einleitungen ( die einfach nicht auf den Punkt kommen…).
Fast ein „Reject-Argument“: Wenn ich feststelle, /dass ein und dieselbe
Arbeit auch in einer anderen Zeitschrift publiziert wurde/, ohne
inhaltlich etwas wesentlich zu verändern (mir mehrfach passiert).

**Peter Koppatz**

Welches Format bevorzugst Du Word, PDF, LibreOffice...?

**Rainer Brüggemann**

Im allgemeinen Word. Wenn ein Verlag es verlangt, dann auch ein pdf-File.

**Peter Koppatz**

Was sollten junge Wissenschaftler heute beherzigen, wenn sie sich an
das Verfassen einer Arbeit wagen?


**Rainer Brüggemann**

a) Kurze Arbeiten
b) Zielführende aber kurze Einleitung
c) Keinen „Gemischtwarenladen“. Interessante Ergebnisse, die aber
   nichts mit der in der Einleitung angegebenen Fragestellung zu tun
   haben, strikt rauslassen.

   - State of art: Bitte nicht bei Galilei, Newton oder Lenin(!) anfangen!
   - Es gibt Reviewer, die negativ bewerten, wenn zu viele Eigenzitate
     vorkommen. Es gibt aber auch Reviewer, die einen Mittelwert aus
     den Jahren zitierter Publikationen berechnen und dann evt. zu
     einer negativen Beurteilung kommen. Ich tue das nicht. Selbst
     wenn mir die Arbeit überhaupt nicht gefällt, und ich auch nicht
     klar komme, wie ich Verbesserungsvorschläge machen kann, versuche
     ich konstruktiv zu sein.
     
**Peter Koppatz**

Vielen Dank für die Informationen.
		



Publikationen lesen/schreiben, aber richtig
===========================================
	   
.. |a| image:: ./images/feder.svg
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     - Weiterführende Literatur zum Lesen und Schreiben ...
	   
- http://www.cs.columbia.edu/~hgs/etc/writing-style.html
- https://www.cl.cam.ac.uk/~mgk25/publ-tips/
-  https://onlinelibrary.wiley.com -> Suche nach »Writing a Paper«
- `How to Read a Paper von S. Keshav <./files/read-and-write/p83-keshavA.pdf>`_
- `Writing reviews for systems conferences von Timothy Roscoe <./files/review-writing.pdf>`_


  

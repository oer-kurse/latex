=======================
 Abkürzungsverzeichnis
=======================

.. _abkuerzungsverzeichnis:

.. INDEX:: Abkürzungen; Verzeichnis
.. INDEX:: Verzeichnis; Abkürzungen

Wer ein Dokument ließt, stolpert vielleicht über Abkürungen und
Acronyme, die er noch nie gesehen oder gehört hat.

Dafür legt man sich eine Liste an, auf die im Text referenziert
werden kann.

In der Präambel
===============

.. code-block:: latex
		
   \usepackage{acronym}
   \usepackage[optionen]{acronym}
   %% Beispiel
   \usepackage[nohyperlinks, printonlyused]{acronym}

Optionen sind dann:

- footnote: die Langform wird als Fußnote ausgeben.
- nohyperlinks: wenn zusätzlich hyperref geladen wird, wird hiermit die automatische Verlinkung verhindert.
- printonlyused: nur Abkürzungen auflisten, die tatsächlich verwendet werden.
- smaller: verkleinert die Anzeige der Abkürzung im Dokument.
- dua: die Ausgabe der Abkürzung erfolgt immer in Langform.
- nolist: es wird kein Verzeichnis aller Abkürzungen erstellt.

Definition/en)
==============

.. code-block:: latex

   \newpage
   \section*{Abreviations}

   \begin{acronym}[ISAM]
     \acro{ALARM}{Acronym: Awareness Labor KMU Informationssicherheit}
     \acro{ISA}{Information Security Awareness}
     \acro{IRT}{Item Response Theory}
     \acro{ISAT}{Information Security Awareness Traing(s)}
     \acro{ISAM}{Information Security Awareness Measurements}
     \acro{ISec}{Information Security}
     \acro{MIS}{Multiindicator System}
     \acro{SME}{small and medium-sized enterprise}
   \end{acronym}


Verweise im Text
================

.. code-block:: latex

   The field of safety (\ac{ISA}) is importend for
   small and medium-sized enterprises (\acp{SME}).


Weiterführende Beispiele
========================

- `Anleitung auf Wikipedia <https://de.wikibooks.org/wiki/LaTeX-W%C3%B6rterbuch:_Abk%C3%BCrzungsverzeichnis>`_

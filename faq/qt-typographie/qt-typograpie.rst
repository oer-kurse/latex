===========
Typographie
===========

.. index:: Typographie
	   
.. |a| image:: ./images/typographie-small.png
   :width: 100px
	   
.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Textsatz ist eine alte Kunst und ungeschriebene Gesetze
       bestimmen das Druckbild vieler Publikationen. Bei der Verwendung eines
       Textverarbeitungsprogrammes werden diese Gesetze oft gebrochen
       und entsprechen gruselig sieht das Ergebnis aus. Latex nimmt einem hier
       schon viel Arbeit ab, aber hin und wieder muss händisch
       nachjustiert werden.

|
Beispiele aus dem Bereich Typographie [#FN1]_

.. image:: images/typographie.png
   :width: 500px

       
Es wird zwischen Mikro- und Makro-Typographie unterschieden. Auch wenn
das Latex-System schon viele grundlegende Antworten auf die Feinheiten
des Druckes gibt, wird man sich mit der Zeit auch mit solchen Fragen
auseinandersetzen (müssen). Hier verweise ich auf Seiten, auf denen
einzelnde Aspekte gut beschrieben sind:

- `The Beauty of LATEX (Kerning, Transparenz, Fonts, Ligaturen, Silbentrennung)
  <http://nitens.org/taraborelli/latex>`_
- `Typografie-Grundlagen
  <https://www.onlineprinters.de/magazin/typografie-grundlagen/>`_
  

Schusterjungen und Hurenkinder
==============================
.. index:: Hurenkinder
.. index:: Schusterjungen

Wie kann ich  »Hurenkinder« (engl. widows), also einzelne Zeilen eines
Absatzes am Kopf von Buchseiten, bzw. »Schusterjungen/Waisenkinder«
(engl. clubs), einzelne Absatzzeilen auf der Seite unten, verhindern?

::

   % Hurenkinder und Schusterjungen verhindern
   \clubpenalty10000
   \widowpenalty10000
   \displaywidowpenalty=10000

   

Siehe auch: http://projekte.dante.de/DanteFAQ/Silbentrennung


  
----

.. [#FN1] `Bild von Brian Ammon, CC BY-SA 3.0 <https://commons.wikimedia.org/w/index.php?curid=2349540>`_

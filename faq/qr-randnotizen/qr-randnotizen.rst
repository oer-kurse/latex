===========
Randnotizen
===========

.. index:: Randnotizen
.. index:: marginpar

.. |a| image:: ./images/randnotiz100.png
	         
     
Beispiel
========
.. sourcecode:: latex

   Das ist eine \marginpar{Kommentar}
   Zeile mit Kommentar am rechten Rand

   
Das Paket *\\usepackage{marginnote}*  bietet weitere Konfigurationmöglichkeiten.

:Beispiel:

   .. image:: ./images/randnotiz.png

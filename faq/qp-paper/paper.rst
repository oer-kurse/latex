===========
Affiliation
===========
.. index:: Affiliation
.. index:: Co-Autoren
	   
.. |a| image:: ./images/sheep.jpg

Lernziel
========

.. list-table::
   :widths: 10 70
   :class: lernziel

   * - |a|
     - Affiliation -- wer hat's erfunden? 
    

Wenn neben den Autoren auch die Institution der Beteiligten publiziert
werden soll, was in wissenschaftlichen Publikationen der Fall ist,
kann dafür das folgende Paket in die Konfiguration aufgenommen werden:
::

   \usepackage{authblk}

In der Präambel kann dann die Liste der Autoren und deren
Institution(en) benannt werden. Ein Beispiel:
::

   \include{a-configuration}
   \title{\huge \frqq PyHasse\flqq \ and cloud computing\\
   \vspace{1em}
   \large \ding{118}\ Partial Order Concepts in Applied Sciences\ \ding{118}}
   \date{2015}

   \author[1)]{Peter Koppatz}
   \author[2)]{Rainer Bruggemann}
   \affil[1)]{Technical University of Applied Sciences Wildau,
              Hochschulring 1, D-15745 Wildau, Germany}
   \affil[2)]{Leibniz-Institute of Fresh Water Ecology and Inland Fisheries
              Department Ecohydrology, Berlin, Germany}
   \begin{document}
   ...

   

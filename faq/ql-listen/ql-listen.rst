======
Listen
======

.. index:: Listen

.. |a| image:: ./files/listen.jpg
   :width: 100px
	   
.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Standard-Listen sehen oft nicht besonders gut aus. Hier ein
       paar Anpassungen.
     
Zeilen-Abstand in der Präambel
==============================

.. sourcecode:: latex	   

   \usepackage{parskip}
   \usepackage{enumitem}
   \setlist[itemize]{parsep=0pt}
   \setlist[enumerate]{parsep=0pt}

Zeilen-Abstand korrigieren
==========================
Direkt in der Liste, den Wert ändern:

.. sourcecode:: latex

   begin{itemize}
   \itemsep -5pt
   \item foo
   \item bar
   \end{itemize}

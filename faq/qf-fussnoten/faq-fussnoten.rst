
=========
Fussnoten
=========

.. index:: Fußnoten
	   
.. |a| image:: ./images/fussnoten.png
   :width: 100px
	   
.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Wenn Fußnoten-Texte länger als eine Zeile werden sieht die
       Darstellung nicht sonderlich schön aus. Mit einem hängenden
       Absatz bekommt man das wieder hint.
       
     
In der Präambel (Konfiguration)
===============================

.. sourcecode:: latex	   

   \makeatletter
   \renewcommand{\@makefntext}[1]{\setlength{\parindent}{0pt}%
   \begin{list}{}{\setlength{\labelwidth}{1.5em}%
   \setlength{\leftmargin}{\labelwidth}%
   \setlength{\labelsep}{3pt}\setlength{\itemsep}{0pt }%
   \setlength{\parsep}{0pt}\setlength{\topsep}{0pt}%
   \footnotesize}\item[\hfill\@makefnmark]#1%
   \end{list}}
   \makeatother
 
Beispiel: Schritt 1
===================
Der originale Fußnotentext...

.. image:: ./images/fussnote1.png


Beispiel: Schritt 2
===================

Nach der Definiton weiter oben ...

Eine überlange URL macht noch das Bild kaputt!


.. image:: ./images/fussnote2.png


Beispiel: Schritt 3
===================
.. index:: URL
	   
Folgender Eintrag in der Prämabel hilft,  denn die überlange URL wird
nun umgebrochen:

.. sourcecode:: latex


   \usepackage{url}
   \makeatletter
   \g@addto@macro{\UrlBreaks}{\UrlOrds}
   \makeatother
 

.. image:: ./images/fussnote3.png

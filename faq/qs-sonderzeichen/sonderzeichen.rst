=======
Symbole
=======

.. index:: Sonderzeichen
.. index:: Symbole


.. |a| image:: ./images/qs-sonderzeichen.png
   :width: 100px


.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     - Mathematische und andere Sonderzeichen finden sich in den folgenden Paketen.
     
In der Präambel (Konfiguration)
===============================

.. sourcecode:: latex

   \usepackage{MnSymbol,wasysym}
 
Beispiele:
::

   \smiley

Noch mehr Symbole findet man in der folgenden PDF:

http://tug.ctan.org/info/symbols/comprehensive/symbols-a4.pdf


===================
 Bilder platzieren
===================
.. INDEX:: Bilder; platzieren
.. INDEX:: platzieren; Bilder

Das Satzsytem von Latex ermittelt die optimale Verteilung von Text und
anderen Obejekten (Bilder, Tabellen).


Präambel
========

.. code-block:: latex

   \usepackage{float}


Anwendung im Text mit [H] für »hier«:

.. code-block:: latex


   \begin{figure}[H]
     \centering
     \includegraphics[width=0.4\textwidth, angle=0]{matching.pdf}
     \caption{Awareness Maturity \ac{SME}.}
     \label{fig:Figure1a}
   \end{figure}

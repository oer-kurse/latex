
.. _svg2tex:

.. meta::

:description lang=de: Dokumente setzen mit Latex -- OER-Kurs auf gitlab.io 
   
:keywords: Latex, Grafiken, OER-Kurs
   
.. |a| image:: ./images/hassediagram.png
   :width: 100px

============
SVG-Grafiken
============

.. index:: images
.. index:: bilder
	   
.. list-table::
   :widths: 10 70

   * - 	|a|

     - Grafiken können in unterschiedlichster Ausprägung eingebunden werden.
       Ein Sonderfall, SVG-Grafiken einbinden, die Probleme und ein Lösungsansatz
       werden hier beschrieben.

Ein SVG in ein Latex-Dokument einbinden gehört nicht zum Standard. Für
solche Fälle gibt es dann Erweiterungen, im konkreten Fall ist es der
Export der Graifk als PDF und das Einbinden als »Grafik«, wie es mit
den Standardbildformaten üblich ist.

Oft empfohlen wird das Paket SVG, dazu muss in der Konfiguration
folgnder Eintrag erfolgen:

::

   %%%%%%%%%%%%%%% SVG-Grafiken mehrfach einginden %%%%%%%%%%%%%%
   
   \usepackage{graphics}
   \graphicspath{{.}}
   \usepackage{svg}

Es wird davon ausgegangen, dass die Bilder auf gleicher Ebenene
liegen, wie die Steuerdatei eines mittelgroßen Latex-Projektes. Über den
zweiten Eintrag in der Konfigurationsdatei kann auch ein alternativer
Suchpfad definiert werden (nicht getestet).

Die SVG-Dateien wurden mit Inkscape erstellt und gespeichert. Für die
Einbindung in ein Latex-Dokument muss aus der SVG-Datei ein PDF und
zusätzlich ein Latexfragment erzeugt werden. Die allgemeine Empfehlung
ist ein Komandozeilenaufruf:

::

   inkscape -D -z -f figure01.svg -A figure01.pdf --export-latex

Es gibt danach drei Dateien:

- figure01.svg, die Ausgangsdatei als Basis
- figure01.pdf, Ergebnis des Kommndozeilenaufrufs
- figure01.pdf_tex, Ergebnis des Kommanodzeilenaufrufs
  
Die Dokumentation zum SVG-Projekt bietet ein Beispiel für die
Einbindung, hier an den konkreten Fall angepasst und ergänzt mit einer
Beschreibung:

::

   \begin{figure}
     \centering
     \def\svgwidth{0.7\textwidth}
     \input{figure01.pdf_tex}    
     \caption{Erstes Hassediagram}
     \label{Figure 1}
   \end{figure}

Soweit, so gut!

Nun ein Blick in den generierten Latex-Quellcode:

.. literalinclude:: files/figure01.pdf_tex
   :language: latex
   :linenos:
   :emphasize-lines: 53-78

Wie in dem farblich hervorgehobenen Bereich zu erkennen ist, hat
Inkscape eine mehrseitige PDF erstellt, was auch gut zu sehen ist,
wenn man die generierte PDF öffnent.

.. image:: ./images/mehrseitige-grafik.png
   :alt: PDF mit Grafik auf mehrere Seiten verteilt

	   
Workaround/Lösung
=================

Weil das mehrseitige PDF und der dazugeörige Quellcode zum Einbinden
nicht wie erwartet funktioniert und die unterschielichsten
Fehlermeldungen produzieren kann, muss  manuell nachjustiert
werden. Das erfolgt in zwei Schritten:

1. Die genierte figure01.pdf_tex wird für eine einseitige PDF umgeschrieben.
   ::

      inkscape -D -z -f figure01.svg -A figure01.pdf --export-latex
   
2. Der Export der SVG wird aus Inkscape heraus wiederholt, was eine
   PDF aus nur einer Seite erzeugt.

   ::

      inkscape -D -z -f figure01.svg -A figure01.pdf
      
3. Manuelle oder mit einem Python-Script werden überflüssige Zeilen aus
   der Datei fiture01.pdf_tex entfernt.

zu 1. Script anpassen
+++++++++++++++++++++

Es müssen alle Zeilen entfernt werden, die sich nicht auf Seite 1
beziehen. Das sind die weiter oben hervorgehobenen Zeilen. Die
reduzierte Datei sieht dann wied folgt aus:


.. literalinclude:: files/figure01a.pdf_tex
   :language: latex
   :linenos:
   :emphasize-lines: 56

zu 2. PDF-Speichern
+++++++++++++++++++

Nun die SVG-Datei nochmal im Inkscape geöffnet und als PDF
gespeichert, erzeugt ein Grafik, die im unterschied zum generierten
PDF aus nur einer Seite besteht und sieht dann wie folgt aus:


.. image:: ./images/pdf-mit-einer-seite.png
   :alt: PDF mit Grafik auf einer Seite zusammengefasst


Nun können die neue PDF (eine Grafik auf einer Seite) und die
angepasste *.pdf_tex in das finale Latex-Dokument eingebunden werden,
wie es die Dokumentation des SVG-Paketes beschreibt. Das funktioniert
dann auch mit mehreren Dateien und alle magischen Fehlermeldungen
verschwinden aus den Log-Dateien....

zu 3. Automatisierung
+++++++++++++++++++++

Für ein Bild kann mit dem oben beschriebenen Verfahren nachjustiert werden.
Sind Bilder mehrfach zu bearbeiten, bietet sich ein Hilfsprogramm an. Die
hier angebotene Lösung nutzt Python.

In Kürze wird hier auf das Script verlinkt.

.. Speichern und Entpacken Sie das Programm.

   Der Erste Aufruf zeigt die Hilfe:

      cd  /zum/ordner/mit/svg-dateien
   pip install svg2tex
   # siehe which inksacpe
   export INKSCAPEPATH = "/usr/bin/inkscape"
   svg2tex -h
   








   



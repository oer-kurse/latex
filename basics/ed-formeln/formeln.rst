=======
Formeln
=======

.. index:: Formeln
	   
.. |a| image:: ./images/struktur.png
   :width: 100px
	   
.. list-table::
   :widths: 10 70

   * - 	|a|

     - Im Formelsatz ist Latex immer noch unschlagbar, versuchen Sie es!


Formelsatz inline, hier werden die Formeln in zwei $-Zeichen
eingeschlossen. Damit lassen sich einzeilge Texte direkt im Fliesstext
unterbringen.

::

   $x^{2}+y^{3}=\ ???$


Website zum erstellen von Fomeln, mit anschließendem Export in diverse
Formate.

- http://www.codecogs.com/latex/eqneditor.php
- http://mathurl.com/

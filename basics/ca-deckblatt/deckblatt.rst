Deckblatt
=========

.. index:: Deckblatt

.. |a| image:: ./images/rueckertbuchdeckel.jpg
   :width: 100px


.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     -  Worum geht es in dem Dokument und wer ist der Autor?  
	Diese Informationen finden wir auf dem Deckblatt oder 
	der Titelseite. Die Konfiguartion und Anwendung der   
	benötigten Meta-Daten wird hier vorgestellt.          

- Im ersten Schritt definieren wir einige Variablen die auf der
  Titelseite verwendet werden.
- Auf der Titelseite lesen wir die in der Konfiguration gesetzten
  Variablen aus.
  
Die wichtigsten Variablen sind:

- Titel
- Autor
- Datum

Die Definition erfolgt, wie alle Konfigurations-Anweisungen in der
zentralen Datei *a-dis-configuration.tex*.

.. literalinclude:: ./images/a-dis-configuration.tex
   :language: latex
   :lines: 3-10
                
Die Verwendung der Variablen aus der Konfigurationsdatei erfolgt dann
auf dem Deckblatt selbst, siehe Datei: *b-deckblatt.tex*.

.. literalinclude:: images/deckblatt.tex
   :language: latex

**Downloads:**

- :download:`deckblatt.tex <./images/deckblatt.tex>`
- :download:`dis-de-deckblatt.pdf <./images/dis-de-deckblatt.pdf>`
- :download:`ZIP-Datei: dis-de.zip <./images/dis-de.zip>`

Wasserzeichen
=============

.. index:: Wasserzeichen
	   
.. |a| image:: ./images/wasserzeichen.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     - Ist das Werk schon vollendet? Um Verwechslungen   
       mit der späteren Endfassung zu vermeiden, markieren wir 
       das Dokument als Entwurf.                               

.. index:: usepackage

Für viele Erweiterungen muss ein zusätzliches Paket eingebunden
werden. Hier wird auch die Grundstruktur eines Latex-Kommandos
sichtbar. Er besteht aus drei Teilen:

- dem Schlüsselwort mit einem vorangestellten Schrägstrich **\\anweisung**,
- den optionalen Parametern in eckigen Klammern **[optional]**,
- und den vorgeschriebenen Parametern in geschweiften Klammern **{pflicht}**.

Wie in Zeile 6 gut zu erkennen ist, kann im Pflichtteil auch wieder
ein anderer Befehl stehen, in dem Beispiel die Ausgabe des aktuellen Datum.

Die Konfiguration enthält dann folgenden Zeilen:

.. literalinclude:: images/a-dis-configuration.tex
   :language: latex
   :lines: 12-16


**Downloads:**

- :download:`a-dis-configuration.tex <./images/a-dis-configuration.tex>`
- :download:`dis-de-wasserzeichen.pdf <./images/dis-de-wasserzeichen.pdf>`




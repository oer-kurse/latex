====================
Besonderheiten in DE
====================
.. index:: eurosym
.. index:: Packages; eurosym
	   
.. |a| image:: ./files/euro-ger.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

	   
   * - |a|

     - Ohne extra Konfiguration verwendet Latex die Einstellugen
       für den englischsprachigen Raum. Mit wenigen Änderungen  
       in der Konfiguration versteht Latex auch die Eigenheiten 
       für Europa und Deutschland besser.                       


Was ist in Deutschland anders?

- das Währungszeichen **€** statt  **$**
- die Umlaute
- das Datumformat
- die Absatzeinzüge
- der Zeilenabstand 
- die Verwendung von Fußnoten
- die Art zu zitieren
- ...

Die vier ersten behandeln wir gleich, die andern bekommen ein extra Kapitel!


Die Eingabe von Umlauten korrekt entgegennehmen. Man ist immer gut
beraten, das utf8-Format zum Speichern von tex-Dateien zu verwenden.

.. literalinclude:: ./files/a-dis-configuration.tex
   :language: latex
   :lines: 28-31
			    
Trennregeln, Datumformat


.. literalinclude:: ./files/a-dis-configuration.tex
   :language: latex
   :lines: 32-42

Euro-Zeichen

.. literalinclude:: ./files/a-dis-configuration.tex
   :language: latex
   :lines: 43-45
		     
Erstzeileneinzug auf 0

.. literalinclude:: ./files/a-dis-configuration.tex
   :language: latex
   :lines: 46-47	     

**Downloads:**

- :download:`a-dis-configuration.tex <./files/a-dis-configuration.tex>`
- :download:`dis-de-german.pdf <./files/dis-de-german.pdf>`
- :download:`dis-de-german.zip <./files/dis-de-german.zip>`

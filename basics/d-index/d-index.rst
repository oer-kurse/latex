======================
 Stichwortverzeichnis
======================

.. index:: index-Erstellung

.. |a| image:: ./images/index.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

       .. code::

          \printindex

     - Wenn das Inhaltsverzeichnis eine grobe Orientierung gibt,
       dann kann mit einem Indexeintrag direkt auf eine gesuchte
       Information verwiesen werden. Wie das vorbereitet wird,  
       zeigt diese Lerneinheit.                                 

Die Arbeitsschritte:

1. Index-Eintrag in den Text einfügen
2. Index-Verzeichnis erzeugen (erster Aufruf)
3. Index an gewünschter Stelle ausgeben
4. PDF erzeugen, der die Seitenzahlen einfügt (zweiter Aufruf)

Ein Indexeitrag wird wie folgt an der betreffenden Stelle (Dokument) eingegeben:
::

   \index{Stichwort}

Indexeinträge sammeln und zusammenführen (Konfiguration):
::

  \usepackage{makeidx}
  \makeindex

Index ausgeben (Anhang), denn die vorherige Anweisungen führen nur die
notwendigen Berechnungen durch, die Einbindung inklusive Ausgabe in
ein Dokument erfolgt mit:
::

   \printindex

Eine Kurzfassung mit allen Befehlen in einer Datei:

.. listing:: latex/d-index/all-in-one.tex latex

PDF-Erstellung
==============
Die Erzeugung des Index erfolgt in einem separaten Lauf und ist
getrennt vom Erstellungsprozess der PDF-Datei. Es sind also mindestens
zwei Druchgänge nacheinander zu starten:

1. Index erzeugen
2. PDF erzeugen

Eventuell sind mehr als ein Aufruf notwendig, weil z. B. die Länge der
Inhaltsverzeichnisse am Anfang des Dokumentes noch nicht korrekt
berechnet werden konnten.

Noch ein Beispiel:

.. literalinclude:: ./files/anhang.tex
   :language: latex
	   
**Download**
   
- :download:`dis-index.pdf: Ergebnis auf Seite 29 <./files/dis-index.pdf>`

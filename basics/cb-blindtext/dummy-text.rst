===========
Dummy-Texte
===========

.. index:: Dummy-Text
.. index:: Blind-Text
	   
.. index:: Packages; lipsum
.. index:: Packages; blindtex

.. |a| image:: ./images/blindtext.png
   :width: 100px


.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Leere Blätter sind nicht schön, mit dem Schreiben haben
       Sie auch noch nicht begonnen! Mit generierten Texten   
       erhalten Sie aber schon mal ein besseres Eindruck über die
       Verteilung der Überschriften. Sie können damit auch den
       ungefähren Umfang Ihrer Arbeit etwas abschätzen.    

Es gibt zwei Pakete für diesen Zweck *blindtext* und *lipsum* [#f1]_. 

.. literalinclude:: ./files/wissensstand.tex
   :language: latex
   :lines: 21-23
	     
Nachfolgend einigen Anwendungsbeispiele:

Kurzer/langer Text
::
   
  \blindtext
  \Blindtext

Kurzes/langes Dokument
::
   
   \blinddocument
   \Blinddocument

Wiederholungen als optionaler Parameter
::
   
   \blindtext[5]

	
Listen aller Art...
::
   
   \blinditemize
   \blindenumerate
   \blindenumerate[10]
   \blinddescription

Mathematische Konstrukte
::
   
   \blindmathtrue
   \blindmathfalse
   \blindmathpaper

::
   
   \lipsum
   \lipsum[3-56]

**Download**:

- :download:`Tex-Datei <./files/wissensstand.tex>`
- :download:`PDF-Datei <./files/dis-de-blindtext.pdf>`
- :download:`ZIP-Datei <./files/dis-de-blindtext.zip>`

----

.. [#f1] `Generating dummy text/blindtext with LaTeX for testing
	 <http://texblog.org/2011/02/26/generating-dummy-textblindtext-with-latex-for-testing/>`_

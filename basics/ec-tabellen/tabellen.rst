========
Tabellen
========

.. index:: Tabellen

.. |a| image:: ./images/tabelle.png
   :width: 100px

.. list-table::
   :widths: 20 70

   * - |a|
     - Tabellen, ob kurz oder lang, sind etwas schwierig zu handhaben.
       Nachfolgend eine Grundvarianten für kurze Tabellen.

    
Erste einfache Version:
::

   \begin{tabular}{rcrcr}
   1  & * & 11 & = & 11\\
   2  & * & 11 & = & 22\\
   3  & * & 11 & = & 33\\
   4  & * & 11 & = & 44\\
   5  & * & 11 & = & 55\\
   6  & * & 11 & = & 66\\
   7  & * & 11 & = & 77\\
   8  & * & 11 & = & 88\\
   9  & * & 11 & = & 99\\
   10 & * & 11 & = & 110\\
   \end{tabular}

Linien erleichtern die Orientierung:
::

   \begin{tabular}{rcrcr}
   1  & * & 11 & = & 11\\
   \hline
   2  & * & 11 & = & 22\\
   \hline
   3  & * & 11 & = & 33\\
   \hline
   4  & * & 11 & = & 44\\
   \hline
   5  & * & 11 & = & 55\\
   \hline
   6  & * & 11 & = & 66\\
   \hline
   7  & * & 11 & = & 77\\
   \hline
   8  & * & 11 & = & 88\\
   \hline
   9  & * & 11 & = & 99\\
   \hline
   10 & * & 11 & = & 110\\
   \end{tabular}

Senkrechte Linien mit der Ausrichtung kombiniert:
::

   \begin{tabular}{|r|c|r|c|r|}
   \hline
   1  & * & 11 & = & 11\\
   \hline
   2  & * & 11 & = & 22\\
   \hline
   3  & * & 11 & = & 33\\
   \hline
   4  & * & 11 & = & 44\\
   \hline
   5  & * & 11 & = & 55\\
   \hline
   6  & * & 11 & = & 66\\
   \hline
   7  & * & 11 & = & 77\\
   \hline
   8  & * & 11 & = & 88\\
   \hline
   9  & * & 11 & = & 99\\
   \hline
   10 & * & 11 & = & 110\\
   \hline  
   \end{tabular}

**Downloads:**

- :download:`tab-ein-mal-eins.tex <./files/tab-ein-mal-eins.tex>`
- :download:`tab-ein-mal-eins.pdf <./files/tab-ein-mal-eins.pdf>`

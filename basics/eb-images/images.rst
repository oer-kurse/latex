.. meta::

:description lang=de: Dokumente setzen mit Latex -- OER-Kurs auf gitlab.io 
   
:keywords: Latex, Grafiken, OER-Kurs
	   
.. |a| image:: ./images/testbild.jpg
   :width: 100px

================
Bilder, Grafiken
================

.. index:: images
.. index:: bilder
	   
.. list-table::
   :widths: 10 70

   * - 	|a|

     - Grafiken können in unterschielichster Ausprägung eingebunden werden.
       Eine Sammlung diverser Varianten, beginnend mit dem einfachsten
       Fall.

Folgende Bildformate lassen sich in ein Latex-Dokument einbinden z.B.:

- eps  
- jpg
- png

- Tiks-Grafiken (extra Lerneinheit)
- EPS-Grafiken (extra Lerneinheit)
- :ref:`SVG einbinden <svg2tex>`

Die Grafik als Ganzes:
======================
Sie muss im gleichen Ordern liegen, wie die tex-Datei.
::

   \includegraphics{graphik.jpg}

Grafik im Unterordner
=====================
::
   
   \includegraphics[width=0.8\textwidth]{images/hassediagram.jpg}

Auf Seitenbreite skalieren
==========================
::
   
   \includegraphics[width=textwidth]{images/hassediagram.jpg}


Prozentual verkleinern
======================
::
   
   \includegraphics[width=0.8\textwidth]{images/hassediagram.jpg}
   

   
Das passende Paket einbinden
============================

.. sourcecode:: latex	   

   \usepackage{graphicx}

Das Bild im Original einfügen
=============================

.. sourcecode:: latex
		
   \includegraphics{testbild.png}
   \includegraphics{testbild.jpg}
   \includegraphics{testbild.pdf}

   % Größe ändern %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   \newpage
   \section{Etwas kleiner...}
   \includegraphics[width=0.7\textwidth]{testbild.png}
   \includegraphics[width=0.3\textwidth]{testbild.jpg}
   \includegraphics[width=0.5\textwidth]{testbild.pdf}

   % zentrieren %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   \newpage
   \section{Bild zentrieren}
   \begin{center}
   \includegraphics[width=0.5\textwidth]{testbild.png}
   \end{center}

   % Beschriftung %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   \newpage
   \section{Bildbeschriftung}
   \begin{figure}[ht]
   \begin{center}
   \caption{Beschriftung oben}
   \label{default}
   \includegraphics[width=0.5\textwidth]{testbild.png}
   \end{center}
   \end{figure}

   % Posionierung beeinflussen %%%%%%%%%%%%%%%%%%%%%%%
   \newpage
   \begin{figure}[ht]
   \begin{center}
   \label{default}
   \includegraphics[width=0.5\textwidth]{testbild.png}
   \caption{Beschriftung unten}
   \end{center}
   \end{figure}

Inkscape
========

Im folgenden Artikel wird beschrieben, wie Zeichnungen die mit
Inkskape erstellt wurden, als Latex-Dokumment exportiert werden
können: https://castel.dev/post/lecture-notes-2/

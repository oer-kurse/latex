========
Gallerie
========

Einige Möglichkeite die mit Latex umgesetzt werden können und über den
reinen Textsatz hinaus gehen.

   
.. tab:: 1

   Symbole
	 
   .. image:: ./files/a_Latex_Beispiele.png     

.. tab:: 2

   Formeln
	   
   .. image:: ./files/b_Formeln.png

.. tab:: 3

   Tabellen
	   
   .. image:: ./files/c_Tabellen.png

.. tab::  4

   Mehrsprachigkeit
	   
   .. image:: ./files/d_Mehrsprachigkeit.png

.. tab:: 5

   Quellcode

  .. image:: ./files/e_Quellcode.png

.. tab:: 6
	 
   Grafiken/Bilder

   .. image:: ./files/f_Mindmap_Tiks.png

.. tab:: 7

   Diagramme
   
   .. image:: ./files/g_UML-Diagramme.png

.. tab:: 8

   Kreuzworträtsel

   .. image:: ./files/h_Kreuzwortraetsel.png

   

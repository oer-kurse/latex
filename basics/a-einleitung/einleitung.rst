=====================
Für wen ist der Kurs?
=====================

.. |a| image:: summe.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
       .. code::

	  \sum
	  
     - LaTeX erleichtert die Arbeit mit demTextsatzsystem TeX. 
       Der Vorteil von LaTeX/TeX besteht darin, dass man sich um
       das Layout wenig kümmern muss. Der Fokus liegt auf den
       Inhalten und der logischen Struktur des Dokuments.
       Auf diese Weise kann man sehr schnell ganze Bücher, Artikel
       oder auch Folien für den Beamer erzeugen,                  
       die allen Regeln der Druckkunst gerecht werden.            


Einige Kriterien
================

- Einsteiger, die eine Schritt für Schritt-Anleitung abarbeiten wollen.
- Angehende Doktoranden die sofort loslegen wollen, weil die Zeit drängt :-)
  Sie sollten sich die fertige Vorlage herunterladen und mit dem
  Schreiben beginnen.
- Für beide Extreme gibt es dann auch reichlich
  Nachschlage-Möglichkeiten, denn die Fragen kommen auf jeden
  Fall. Probleme die ich schon mal gelöst habe  sind hier auf jeden
  Fall dokumentiert und kürzen die Suche im Internet eventuell etwas
  ab. Vielleicht hilfts ...

Pro und Kontra
==============

Es gibt viel Pro und Kontra zum Einsatz von Latex und die Diskussionen
der extremen Lager, von totaler Ablehnung bis zu uneingeschränkten
Liebe, sind dann oft sehr emotional. Wie immer heißt es ruhig
Blut bewahren und nach reichlicher Überlegung eine Entscheidung
dafür oder dagegen zu fällen.

Nachfolgend einige Pro- und Kontra-Punkte von mir:

Pro
---

- Latex-Dokumente sehen als PDF einfach schön und elegant aus, das
  liegt vor allem an der ausgefeilten Mikro- und Makrotypographie.
- Die Quellen, also der Text und die Bilder lassen sich in einer
  Versionsverwaltung elegant managen. Man kann im Extremfall nach
  hunderten Änderungen zu jeder vorherigen Version zurückkehren oder
  damit vergleichen.
- Für die Verwaltung von Referenzen, Indexeinträgen,
  Verzeichnissen brauchen sie sich wirklich keine Gedanken mehr zu
  machen, die Seitenangaben stimmen immer, egal wie oft sie die
  Struktur der Dokumente ändern!
- Wer Formeln braucht, keine Frage, da sind sie mit Latex immer noch
  Weltmeister!
- Arbeiten am Inhalt und Änderungen am Ausgabeformat lassen sich
  wunderbar voneinander trennen. 

Kontra
------

- Markup: Wer es nicht kennt ist erst einmal irritiert. Als
  gestandener Entwickler hab ich damit aber kein Problem. Nach kurzer
  Eingewöhnungsphase hat man eine Schalter im Kopf der entweder nur
  das Markup zeigt, weil man eine Steueranweisung sucht oder eingeben
  will oder nur den Text, weil man am Inhalt arbeitet...
- Ja, die vielen Steuerkommandos hat man schnell wieder vergessen,
  aber im Netz und in diesem Kurs ist schnell wiedergefunden, was man
  so braucht. Wie bei jeder Sprache braucht man nur eine kleine
  Teilmenge aus der schier unerschöpflichen Auswahl.

Wenn Sie mir immer noch nicht glauben, dann finden Sie nachfolgend
weitere, teils kontroverse Diskussionen/Wortmeldungen:

- Auf meinem PC läuft zwar Windows XP und ich habe sogar MS Word
  installiert, aber ich will ja eben deswegen Latex verwenden, weil ich
  es satt habe, dass Word mir das Inhaltsverzeichnis zerpflückt und
  standardmäßig bei Dokumenten ab 70 Seiten die Formatierung gar nicht
  mehr hin bekommt. Open Office finde ich nebenbei auch nicht viel
  besser... Außerdem finde ich die Einbindung von BibTex in Latex sehr,
  sehr komfortabel... [#f1]_

- Argumente für Latex (in Englisch) [#f2]_

- Hier noch ein paar Bildschirmfotos, die zeigen, was neben der
  Textgestaltung noch alles möglich ist.
  
  `Bildschirmfotos - eine Auswahl...
  <./slideshow.html>`_
  
..  @startuml
    :Start;
    :Strategie?;
    split
     :Muster-Vorlage nutzen;
     :anpassen;
    split again
     :Vorlage selber erstellen;
     :Schritt für Schritt;
    split again
     :Problem;
     :Nachschlagen;
    end split

    :PDF generieren;
    @enduml

Möglichkeiten, den Kurs zu nutzen...
------------------------------------

.. image:: strategie.svg
	   
.. [#f1] http://user.uni-frankfurt.de/~muehlich/tex/wordvslatex.html (nicht mehr verfügbar)
.. [#f2] http://www.andy-roberts.net/writing/latex/benefits	 





Verzeichnisse
=============

.. index:: Verzeichnisse generieren
	   
.. |a| image:: verzeichnisse.png
   :width: 100px


.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Den ersten Überblick zur Struktur erhält man durch die 
       unterschiedlichsten Verzeichnissse. Damit ist eine     
       gezielte Suche, neben der Nutzung eines Index gegeben. 


Folgende Verzeichnisse können automatisch erstellt werden:

- Abbildungsverzeichnis (\\listoffigures)
- :ref:`Abkürzungsverzeichnis <abkuerzungsverzeichnis>`
- Inhaltsverzeichnis (\\tableofcontents)
- :ref:`Literaturverzeichnis <literaturverzeichnis>`
- Tabellenverzeichnis (\\listoftables)

 
Der Inhalt der Datei  *verzeichnisse.tex* sieht nun wie folgt aus:

.. code-block:: latex
   
   \chapter{Inhaltsverzeichnisse}
   
   \tableofcontents
   \listoffigures
   \listoftables




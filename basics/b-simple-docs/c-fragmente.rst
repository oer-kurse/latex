===============
Fragementierung
===============

.. |a| image:: ./files/fragmente.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     -  Die Inhalte einfacher Dokumente können in einer Datei      
	verwaltet werden. Für komplexe Werke wie Bücher,           
	Projektanträge oder Dissertationen ist es eher unpraktisch.
	Der Entwurf kann auch über Ordner und Daeien verteilt
	in kleine Teile zerlegt und verwaltet werden.

- Ein Präfix aus Buchstaben, erleichtert die sortierte Anzeige
  im Dateisystem.
- Die Dokumentenklasse (die bisher einzige Konfigurations-Anweisung)
  verschieben wir in eine Datei *a-dis-configuration.tex*
- Jedes Kapitel mit Überschrift verlegen wir in einen extra Ordner.
- In der Hauptdatei *a-dis-main.tex* führen wir alle Fragmente, durch
  *\\input*-Anweisungen wieder zusammen.
 
Die Überarbeitete Hauptdatei sieht dann wie folgt aus:

.. listing:: latex/b-simple-docs/a-dis-main.tex latex

Die dazugehörige Konfigurationsdatei mit dem Einzeiler...

.. listing:: latex/b-simple-docs/a-dis-configuration.tex latex

Die Ordnerstruktur sieht wie folgt aus:

.. image:: ./files/ordnerstruktur1.png
   :width: 300px

nochmal mit den darin enthaltenen Dateien:
	   
.. image:: ./files/ordnerstruktur2.png
   :width: 300px


Beide Anweisungen fügen die Datei an der betreffenden Stelle im
Hauptdokument ein. Die *\\include*-Anweisung erzeugt aber immer einen
Seitenumbruch! Das Einfügen von Dateien aus der Ordnerhireachie
oberhalb der Hauptdatei bereitete des öfteren Probleme.


Unterschied: include -- input
=============================

input
-----
Bindet vor dem Lauf die tex-Datei in das Hauptdokument ein. Es erlaubt
den relativen Import von Dateien.

include
-------

Erlaubt weitere Konfigurationen und erzeugt für jede Datei eine extra
aux-Datei. Dies erlaubt die Übersetzung von Teilen einer größeren
Arbeit ohne die Referenzen durcheinander zu bringen!
Möglich sind dann Befehle wie:

- \\excludeonly
- \\includeonly

Mit relativen Pfadangaben konnte mein System keine aux-Dateien
erzeugen. Ein Wechsel von *include* zu *input* war die Lösung des
Problems.

.. index:: Download; Musterstruktur für Belegarbeit
	   
**Download:**
   
   :download:`Ordnerstruktur als zip-Datei
   <./files/dis-start-ordner.zip>`

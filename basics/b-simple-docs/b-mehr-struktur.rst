Mehr Struktur
=============

.. |a| image:: images/struktur.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

       .. code::

          \equiv  \equiv 

     - Latex bricht die Zeilen im Quelltext nicht um. Wie beim 
       HTML wird eine hierarchische Struktur durch Auszeichnungen
       (Anweisungen) erzeugt. An erster Stelle stehen hier die
       Gliederungsebenen mit den Überschriften.

- Die Dokumentenklasse wechselt von *article* zu
  *book*, denn die Artikel-Klasse kennt keine Kapitel als
  Struktur- bzw. Gliederungselement.
- Jede Kapitel-Überschrift wird zu einem Parameter der
  *\chapter*-Anweisung, des halb die Verwendung der geschweiften Klammern.
- Jedes Kapitel (chapter) beginnt automatisch auf einer neuen Seite.

Die Gliederungsebenen im Vergleich zu den Dokumenten-Klassen.

.. list-table::
   :widths: 30 20 20
   :class: lernziel	    

   * - Dokumenten-Klasse
     - **book**, **report** 
     - **article**
   * - Gliederungsebene -1
     - \\part
     - 
   * - Gliederungsebene 0
     - \\chapter
     - 
   * - Gliederungsebene 1
     - \\section
     - \\section
   * - Gliederungsebene 2
     - \\subsection
     - \\subsection
   * - Gliederungsebene 3
     - \\subsubsection
     - \\subsubsection
   * - Gliederungsebene 4
     - \\paragraph
     - \\paragraph
   * - Gliederungsebene 5
     - \\subparagraph
     - \\subparagraph

Am häufigsten eingesetzt werden die Ebenen 0 - 3, die anderen Ebenen
eher selten.

Beispiel:

.. code-block:: latex

   \documentclass{book}

   \begin{document}
   \chapter{Deckblatt}
   \chapter{Inhaltsverzeichnis(se)}
   \chapter{Einleitung}
   \chapter{Stand des Wissens}
   \chapter{Eigener Loesungsansatz}
   \chapter{Ergebnisse}
   \chapter{Einordnung des eigenen Ansatzes und der Ergebnisse}
   \chapter{Zusammenfassung und Schlussbemerkung}
   \chapter{Anhaenge}
   \chapter{Literaturverzeichnis}
   \end{document}

**Download:**
   
- :download:`Quellcode: struktur-03.tex <files/struktur-03.tex>`
- :download:`Ergebnis:  struktur-03.pdf <files/struktur-03.pdf>`

Hier noch eine Grundstruktur aus dem englischsprachigen Raum:

.. code-block:: latex

   \documentclass{book}

   \begin{document}
   \chapter{Dedication}
   \chapter{Acknowledgements}
   \chapter{Table of Contents}
   \chapter{List of Tables}
   \chapter{List of Figures}
   \chapter{List of Abbreviations}
   \chapter{Abstract}
   
   \chapter{Introduction}
   \chapter{Review of the literature}
   \chapter{Conceptual framework and contextual paradigm}
   \chapter{Research methodology}
   \chapter{Institutional data and analysis}
   \chapter{Discussion of the findings}
   \chapter{Conclusion and recommendations}
   \chapter{References}
   \chapter{Appendices}
   
   \end{document}

**Download:**
   
- :download:`Quellcode: struktur-04.tex <files/struktur-04.tex>`
- :download:`Ergebnis:  struktur-04.pdf <files/struktur-04.pdf>`

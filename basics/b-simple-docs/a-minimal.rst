Das Grundprinzip
================

.. |a| image:: latex.png
   :width: 150px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
       .. code::

	  \latex

     - Die folgende Übungsserie zeigt den Aufbau eines Latex-Dokuments
       vom minimalistischen Ansatz, bis zu einer Struktur die für
       Arbeiten jeden Umfangs geeignet ist.
      


Man erstellt eine neue Datei mit der Endung *tex*. Die folgenden vier
Zeilen bilden das Gerüst für die erste und einfachste PDF-Datei:

.. code-block:: latex
   :linenos:
      
   \documentclass{book}
   \begin{document}
     Hallo Latex ich komme...
   \end{document}

Ein Latex Dokument ist immer in zwei Teile geteilt, der Präambel (hier
Zeile 1 mit der Festlegung zur Dokumenten-Klasse), die für die
Konfiguration verwendet wird und dem Dokument mit den Inhalten, also
alles was zwischen \\begin{document} und \\end{document} steht.

Auch die Grundstruktur der Latex-Anweisungen sind hier zu erkennen:

- Anweisungen beginnen mit einem  \\ gefolgt vom Schüsselwort ( hier
  document, begin und end). 
- Aufrufparameter werden in geschweiften Klammern eingesetzt und sind
  oft Pflichtangaben. Sind keine Klammern+Parameter notwendig
  reduziert sich der Befehl auf das Schlüsselwort nach dem Schrägsrich (\\).
  
Strukturelemente einer wissenschaftlichen Abhandlung (etwas mehr
Text):

.. code-block:: latex
   :linenos:
      
   \documentclass{book}

   \begin{document}
   Deckblatt
   Inhaltsverzeichnis(se)
   Einleitung
   Stand des Wissens
   Eigener Loesungsansatz
   Ergebnisse
   Einordnung des eigenen Ansatzes und der Ergebnisse
   Zusammenfassung und Schlussbemerkung
   Anhaenge
   Literaturverzeichnis
   \end{document}

Wenn Sie aus dem Quelltext eine PDF generieren, dann beachten Sie,
Latex ignoriert die Zeilenumbrüche im Quelltext!

**Download**
   
- :download:`Quellcode: struktur-01.tex <files/struktur-01.tex>`
- :download:`Ergebnis:  struktur-01.pdf <files/struktur-01.pdf>`


Hier noch eine Grundstruktur aus dem englischsprachigen Raum:

.. code-block:: latex

   \documentclass{book}

   \begin{document}
   Dedication
   Acknowledgements
   Table of Contents
   List of Tables
   List of Figures
   List of Abbreviations
   Abstract

   Introduction
   Review of the literature
   Conceptual framework and contextual pradigm
   Research methodology
   Institutional data and analysis
   Discussion of the findings
   Conclusion and recommendations
   References
   Appendices
   
   \end{document}

**Download:**

   
- :download:`Quellcode: struktur-02.tex <files/struktur-02.tex>`
- :download:`Ergebnis:  struktur-02.pdf <files/struktur-02.pdf>`


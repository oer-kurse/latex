========
 Listen
========
.. index:: Listen
.. index:: Listen; description
.. index:: Listen; enumeration
.. index:: Listen; itemize
	   
.. |a| image:: ./images/listen.jpg
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Listen, die Standardversionen ... 

Drei Grundvarianten stehen für Listen zur Verfügung:

- Definitionlisten (description)
- Nummerierte Listen (enumeration)
- Aufzählung (itemize)

Listentyp: description
======================
(Alternative kann ein Glossar genutzt werden!)

.. sourcecode:: latex
   
   \begin{description}
     \item[ PyHasse] Softwarepaket
     \item[ mis] Abkürzung für Multi Indikator System
     \item[ PO] Abkürzung für Partial Order
   \end{description}


Ergebnis (als Bild):

.. image:: ./images/description.jpg
   :width: 300px
	   
Listentyp: enumeration
======================
.. sourcecode:: latex
   
  \begin{enumerate}
    \item Installieren Sie Python (http://www.python.org).
    \item Legen Sie ein neues \flqq virtuelles Environment\frqq \ an.
    \item Aktivieren Sie das \flqq virtuelle Environment\frqq.
    \item Installieren Sie weitere Pakete mit pip install \dots 
  \end{enumerate}

Ergebnis (als Bild):

.. image:: ./images/enumeration.jpg
   :width: 400px
      
Listentyp: itemize
==================
.. sourcecode:: latex
   
   \begin{itemize}
     \item Alpha ($\alpha$)
     \item Beta ($\beta$)
     \item Psi ($\phi$)
     \item Delta ($\delta$)
   \end{itemize}

Ergebnis (als Bild):

.. image:: ./images/itemize.jpg
   :width: 150px


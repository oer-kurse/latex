======================
 Literaturverzeichnis
======================

.. _literaturverzeichnis:

.. INDEX:: Verzeichnis; Literatur
.. INDEX:: Literatur; Verzeichnis

Präambel
========

.. code-block::

   \usepackage[%
     backend=bibtex      % biber or bibtex
   %,style=authoryear    % Alphabeticalsch
    ,style=numeric-comp  % numerical-compressed
    ,sorting=none        % no sorting
    ,sortcites=true      % some other example options ...
    ,block=none
    ,indexing=false
    ,citereset=none
    ,isbn=true
    ,url=true
    ,doi=true            % prints doi
    ,natbib=true         % if you need natbib functions
   ]{biblatex}
   \addbibresource{bibliography}  % better than \bibliography

Zitieren im Text
================

Meine Quellen sind: \\cite{JupyterWeb, eRm} und MIS \\cite{BrgPatil2011} ...


Bibtex-Beispiele
================

In der Datei bibliography.tex sind die genannten Quellen aufgelistet:


.. code-block:: latex

   @book{BrgPatil2011,
     author       = {Bruggemann, R. and Patil, B. P.},
     title        = {{Ranking and Prioritization for Multi-indicator Systems:
                     Introduction to Partial Order Applications}},
     year         = {2011},
     publisher    = {Springer},
     address      = {New York, NY}
   }

   @online{JupyterWeb,
     author		= {Community},
     title		= {{Jupyter Software}},
     keywords		= {Software},
     url                = {https://jupyter.org},
     year		= {2022},
     adsnote		= {Open Source},
     urldate            = {2023-01-30},
   }
   
   @misc{eRm,
     author		= {Mair, P. and Hatzinger R. and Mair, M.J.},
     title		= {{eRm: extended Rasch modelling}},
     version            = {Version: 1.0-2},
     keywords		= {Software, R},
     url		= {https://cran.r-project.org/web/packages/eRm/index.html},
     year		= {2022},
     adsnote		= {Open Source},
     urldate            = {2022-10-22},
   }

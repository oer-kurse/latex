=====
LATEX
=====

Dies ist ein Kurs für das Textsatz-System Latex. Es ist für Einsteiger
und als Nachschlagewerk gedacht.

Installation
============
Wenn der Kurs local benutzt werden soll, muss eine Sphinx-Doku
initialisiert werden:

.. sourcecode:: bash

   sphinx-quickstart latex


Quellcode kopieren
==================

.. sourcecode:: bash

   cd latex
   rm -rf source
   https://gitlab.com/oer-kurse/latex.git source

HTML generieren
===============

.. sourcecode:: bash

   make html

Doku ansehen
============
Das fertig generierte HTML liegt im Ordner *build* und kann im Browser
mit der *index.html* geöffnet werden.
Möglich ist auch der Start eines Server:

.. sourcecode:: bash

   cd build
   python -m http.server

Im Browser dann http://localhost:8000/ eingeben.
   


   

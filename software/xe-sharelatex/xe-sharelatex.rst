====================
Sharedlatex (online)
====================

.. index:: Sharedlatex
	   
.. |a| image:: ./images/sharelatex.png
   :width: 200px


.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     - Wer die Installation scheut oder gemeinsam an einem Dokument arbeiten möchte...

       Das kann alternativ auch über ein Repository (Git) realisiert werden.

**Website:** `ShareLatex <https://www.sharelatex.com>`_

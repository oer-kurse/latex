.. |a| image:: ./images/texshop.png
   :width: 100px
	   
TexShop (MacOS)
===============

.. index:: Editor; TexShop

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - TexShop ist ein Editor für die Bearbeitung und Erstellung von
       Latex-Dokumenten auf einem Apple-System. Wer es einfach haben will,
       installiert sich das mactex-Paket (siehe Link am Ende).

       
Wer es bei der Kombination *Tex Live & Tex-Shop* belassen will bekommt
folgende Informationen und muss noch ein wenig Konfigurationsarbeit
leisten.

.. image:: ./images/pfade-nach-installation.jpg
   :width: 500px

|  

.. image:: ./images/pfade-in-texshop.jpg
   :width: 500px

|

:MacTex:
   
   `Rundum-Glücklich-Paket 
   <http://tug.org/mactex/mactex-download.html>`_  

	   

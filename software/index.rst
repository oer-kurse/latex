==========
 Software
==========

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Software:

   xd-tex-live/*
   xe-emacs/*
   xe-techstudio/*
   xe-texniccenter/*
   xe-texshop/*
   xe-sharelatex/*
   

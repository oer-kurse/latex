.. |a| image:: ./images/tex-live.png
   :width: 100px
	   
Tex Live Installation
=====================

.. index:: Tex Live

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Die Basis für alle Tex-Anwendungen ist Tex selbst mit allen
       Erweiterungen, die im Paket *Tex Live* gebündelt sind.
       
Alle Tex-Editoren brauchen "Tex live" und dieses Paket wird **vor** dem Editor
installiert bzw. das verwendete Installationspaket erledigt diesen
Arbeitsschritt gleich mit.


:Windows:
   `Eine deutschsprachige Anleitung
   <http://www.dante.de/tex/tl-install-windows.html>`_
   
:Website/Download:
   `Tex Live
   <https://www.tug.org/texlive/acquire-netinstall.html>`_

:MacOS:
   `Rundum-Glücklich-Paket 
   <http://tug.org/mactex/mactex-download.html>`_ 

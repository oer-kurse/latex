
.. |a| image:: ./images/emacs.png
   :width: 100px
	   
Emacs (alle Systeme)
====================

.. index:: Editor; Emacs

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|

     - Für Minimalisten, etwas exotisch, dafür aber extrem flexibel.
       
:Website: `Convenient Emacs Setup for Social Scientists
	  <http://www.iq.harvard.edu/news/convenient-emacs-setup-social-scientists-available-thanks-rtc-team-member>`_
:Repository: https://github.com/izahn/dotemacs
  

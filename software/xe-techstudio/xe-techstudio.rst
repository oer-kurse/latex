===================
Texstudio (Windows)
===================

.. index:: Editor; Texstudio

.. |a| image:: ./images/texstudio128x128.png
   :width: 100px


.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     - Sie benötigen die neueste Latex-Version:

       - »Tex live« und
       - einen Editor zum Editieren von tex-Dateien.


Website
=======
Laden sie die unten genannten Programme und installieren Sie diese in
der genannten Reihenfolge:

1.   Download: `Tex Live <https://www.tug.org/texlive/acquire-netinstall.html>`_
2.   Download: `Programm zum Mounten von ISO-Dateien (nur Windows)
     <http://deamontools.cc/download>`_
     und damit ISO aus Punkt 1 einbinden.

     .. image:: ./images/deamantools.png
        :width: 300px

3.   Tex live installieren (siehe README)

     .. image:: ./images/techlive-install.png
        :width: 300px
    
4.   Download:  `Tex-Studio <http://www.texstudio.org>`_
5.   Tex-Studio installieren...

Problem mit Pfaden
==================

Sollten Sie eine Fehlermeldung erhalten, müssen sie im Menü, über
Optionen,... den Pfad zur latex.exe bzw pdflatex.exe noch einmal
definieren.

.. image::  ./images/texstudio-pfade.png
   :width: 500px	    


	   

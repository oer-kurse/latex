TeXnicCenter (Windows)
======================

.. index:: TeXnicCenter
	   
.. |a| image:: ./images/txc.png
   :width: 100px

.. list-table::
   :widths: 10 70
   :class: lernziel	    

   * - |a|
     
     - Für die Bearbeitung von Latex-Dokumenten werden unter 
       Windows mindestens zwei Programm benötigt.
       
       1. Eine IDE (Integrated Development Environment ) und 
       2. ein PDF-Konverter.                                 

Die Installation der IDE wird hier beschrieben, in der
nächsten Station die Installation des PDF-Konverter.  


- Laden Sie das Programm von folgender Internet-Adresse herunter:
  http://www.texniccenter.org
- Installieren Sie die Software 

Nachfolgend sehen sie die Folge der Bildschirminhalte die wärend der
Installation eingeblendet werden. In den meisten Fällen können Sie die
Standardeinstellungen übernehmen.

.. figure:: ./images/install-01-start-wizzard.png
    :alt: wizzard

    Start des Wizzard...
       
.. figure:: ./images/install-02-lizenz.png
    :alt: wizzard

    Lizenz akzeptieren...

.. figure:: ./images/install-03-zielordner.png
    :alt: wizzard

    Lizenz akzeptieren...

.. figure:: ./images/install-04-komponenten.png
    :alt: wizzard

    Lizenz Komponenten auswählen...

.. figure:: ./images/install-05-menue.png
    :alt: wizzard

    Menü-Eintrag...

.. figure:: ./images/install-06-fertig.png
    :alt: wizzard

    Fertig

.. figure:: ./images/install-06-zusammenfassung.png
    :alt: wizzard

    Zusammenfassung

Nun können sie die Installation beginnen und anschließend das Programm
im Menü auswählen und starten.


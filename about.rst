.. _about:

=============
Über den Kurs
=============
	   


Das Schulungsmaterial kann zum Nachschlagen, für das Selbstlernen und
andere Schulungskonzepte verwendet werden.

Lizenz
======

.. image:: ./images/CC-BY.png
.. image:: ./images/oer-logo.png


Die OER-Lizenz bedeutet, dass mit den Inhalten folgende Aktionen
durchgeführt werden dürfen:


- ungefragt verwenden
- ungefragt verändern
- ungefragt mit anderen Inhalten neu vermischen

Es gelten auch die Regeln der Creative Commons Lizenz:

Sollten diese Lizenzen immer noch für Unsicherheiten bezüglich der
Nutzung hervorrufen, erkläre ich hiermit einen Nichtangriffspackt und
verzichte auf alle rechtlichen Mittel, da es nichts durchzusetzen
gibt.

Wie Komme ich an den Quellcode?
===============================

Eine zweite Möglichkeit besteht im Download des kompletten Quellcodes,
der unter folgender URL zur Verfügung gestellt wird:

Kursmaterial auf Bitbucket.org

Was ist rst und Sphinx?
=======================

Sphinx ist eine Software, die für die Dokumentation von
Python-Projekten verwendet wird. Das Standard-Datei-Format ist rst, es
handelt sich um ein einfaches Textformat, welches durch Markup
ergänzt, aus einer Quelle, diverse Ausgabe-Formate generieren kann z.B
HTML, Latex (als Vorstufe für die Generierung eines PDF).

Siehe auch die Dokumentation zum Projekt: Sphinx-Dokumentation

Über den Autor
==============

Ich bin seit vielen Jahren als Dozent tätig. Für meine Kurse stelle
ich Materialien zur Verfügung, nun auch als OER. Neben der Entwicklung
von Kursmaterialien, interessiere ich mich für die Programmiersprache
Python, lese gern und halte mich mit Sport fit.

.. image:: ./images/peter-koppatz.jpg
